## Azure Auth Test in Ionic App

this project has been created following the [How to use Apache Cordova client library for Azure Mobile Apps](https://docs.microsoft.com/en-us/azure/app-service-mobile/app-service-mobile-cordova-how-to-use-client-library)

### requirements

Install *ionic 2* - [Gettig started](http://ionicframework.com/getting-started/) 

### steps for IOS

1. clone repo in <your_dir>
2. cd <your_dir>
3. run `npm install`
4. run `ionic state reset`
5. run `ionic platform add ios`
6. run `ionic build ios`
7. open project  `/platfrom/ios/azure-auth-test.xcodeproj` using XCode 
8. run project

