import { Component, OnInit } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';

declare var WindowsAzure: any;

@Component({
  templateUrl: 'app.html'
})
export class MyApp /*implements OnInit*/ {
  rootPage:any = TabsPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      alert( "START LOGIN!!!");
      try {
        this.login();      
      }
      catch( e ) {
        alert( "ERROR " + e);
      }
    });
  }


   login(): void {

    var client = new WindowsAzure.MobileServiceClient("https://rxkmservicetsf.azurewebsites.net");
    client.login("aad")
        .done(function (results) {
           alert("You are now logged in as: " + results.userId);
      }, function (err) {
      alert("Error: " + err);
    });
   }
}
